export interface IAccordionData {
  description: string,
  faqid: number,
  faquid: string,
  groupID: number,
  id: string,
  language: number,
  priority: number,
  title: string,
}
