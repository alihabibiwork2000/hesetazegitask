export interface IAccordion {
  text: string;
  content: string;
  number: number;
}

export interface ListItem {
  title: string;
  description: string;
}
