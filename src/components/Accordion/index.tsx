import * as React from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { IAccordion } from "../../interface/accordion";
import "./Accordion.scss"

const SimpleAccordion : React.FC<IAccordion> = ({text,content,number}) => {

  return (
    <>
      <Accordion className="accordion">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          className="accordion-header"
        >
          <Typography>
            {number}. {text}
          </Typography>
        </AccordionSummary>
        <AccordionDetails className="accordion-detail">
          <Typography
            dangerouslySetInnerHTML={{ __html: content }}
          ></Typography>
        </AccordionDetails>
      </Accordion>
    </>
  );
}

export default SimpleAccordion;