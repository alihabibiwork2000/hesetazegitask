import React, { FC, useEffect, useState } from "react";
import "./questions.scss";
import { Button } from "@mui/material";
import { toast } from "react-toastify";
import ShareOutlinedIcon from "@mui/icons-material/ShareOutlined";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import axios, { AxiosResponse } from "axios";
import SimpleAccordion from "../../components/Accordion";
import Spinner from "../../components/Spinner";
import { ListItem } from "../../interface/accordion";

const QuestionsPage: FC = (): JSX.Element => {
  const [list, setList] = useState<ListItem[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    axios
      .get("https://api-dev.hesetazegi.com/FAQ/List")
      .then((res: AxiosResponse<{ content: { items: ListItem[] } }>) => {
        setLoading(true);
        setTimeout(() => {
          setList(res.data.content.items);
          setLoading(false);
        }, 1000);
      })
      .catch((err) => {
        toast.error(err.message, {
          position: toast.POSITION.TOP_CENTER,
        });
        setLoading(false);
      });
  }, []);

  const renderItems = () => {
    if (loading) {
      return <Spinner />;
    }
    let breakNumber = Math.round(list.length / 2);
    return list.map((item: ListItem, index: number) => {
      return (
        <React.Fragment key={index}>
          <li className="item">
            <SimpleAccordion
              text={item.title}
              content={item.description}
              number={index + 1}
            />
          </li>
          {breakNumber === index + 1 && <li className="break-items"></li>}
        </React.Fragment>
      );
    });
  };

  return (
    <>
      <div className="container">
        <div className="header">
          <div className="header-title">
            <h1>سوالات متداول</h1>
            <div>
              <span>صفحه اصلی</span>
              <KeyboardArrowLeftIcon />
              <span>سوالات متداول</span>
            </div>
          </div>
          <Button className="share-icon">
            <ShareOutlinedIcon />
          </Button>
        </div>
        <div className="main">
          <ul className="accordion-wrapper">{renderItems()}</ul>
        </div>
      </div>
    </>
  );
};

export default QuestionsPage;
