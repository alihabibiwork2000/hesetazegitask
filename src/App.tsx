import './App.scss';
import QuestionsPage from './pages/Questions';
import { createTheme, ThemeProvider } from "@mui/material";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {

  const theme = createTheme({
    typography: {
      fontFamily: "iran-yekan-farsi"
    },
  });

  return (
    <>
      <ThemeProvider theme={theme}>
        <QuestionsPage />
        <ToastContainer />
      </ThemeProvider>
    </>
  );
}

export default App;
